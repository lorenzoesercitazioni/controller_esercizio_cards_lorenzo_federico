<x-layout>
    <div class="container">
       
        <div class="row">         
            <div class="col-12 col-md-3">
            <x-card
            title="{{$announcement['title']}}"
            description="{{$announcement['description']}}"
            price="{{$announcement['price']}}"
            img="{{$announcement['img']}}"
            />          
            </div>                   
        </div>
             
    </div>
</x-layout>   
