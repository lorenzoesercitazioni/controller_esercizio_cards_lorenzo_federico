@props(['title','description', 'price', 'img'])
<div class="card">
    <img src="{{$img}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$title}}</h5>
      <p class="card-text">{{$description}}</p>
      <p class="card-text">{{$price}}</p>
      @if (Route::currentRouteName()=='annunci')
      <a href="{{route('annunci.specifica', ['title'=>$title])}}" class="btn btn-danger">Specifica Annuncio</a>         
      @else
      <a href="{{route('annunci')}}" class="btn btn-success">Torna agli annunci</a>        
      @endif
      
    </div>
</div>