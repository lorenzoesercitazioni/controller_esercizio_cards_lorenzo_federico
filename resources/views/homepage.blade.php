<x-layout>
    <header class="masthead">
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            
              <div class="col-12 col-md-8 text-center">
                <h1 class="font-weight-light">PRESTO</h1>
                <p class="lead">Sito di Annunci</p>
              </div>
              <div class="col-12 col-md-4">
                  <img class="img-fluid" src="/img/digital.jpg" alt="">
              </div>
          </div>
        </div>
    </header>
    
</x-layout>