<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home() {
        return view('homepage');
    }

    public function announcements(){
        $announcements = [
            ['title'=>'inter', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/interlogo.jpg', 'price'=>'2000'],
            ['title'=>'juventus', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/juventuslogo.jpg', 'price'=>'180'],
            ['title'=>'milan', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/milanlogo.jpg', 'price'=>'250'],
            ['title'=>'lazio', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/laziologo.jpg', 'price'=>'2440'],
            ['title'=>'atalanta', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/atalantalogo.jpg', 'price'=>'345'],
        ];
        return view('annunci',['announcements'=>$announcements]);
    }

    public function dettagli($title){
        $announcements = [
            ['title'=>'inter', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/interlogo.jpg', 'price'=>'2000'],
            ['title'=>'juventus', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/juventuslogo.jpg', 'price'=>'180'],
            ['title'=>'milan', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/milanlogo.jpg', 'price'=>'250'],
            ['title'=>'lazio', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/laziologo.jpg', 'price'=>'2440'],
            ['title'=>'atalanta', 'description'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, error architecto quos soluta consectetur quisquam cupiditate earum nesciunt ducimus ut esse minima laboriosam distinctio repellendus cumque saepe, temporibus obcaecati harum!', 'img'=>'/img/atalantalogo.jpg', 'price'=>'345'],
        ];
        foreach ($announcements as $announcement){
            if($announcement ['title']== $title){
                return view('specifica' , ['announcement' => $announcement]);

            }
        }
        
    }
}
